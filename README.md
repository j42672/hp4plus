# HyPer4+: the platform for network slicing research

[HyPer4][1] is a *portable* and *dynamic* solution for virtualizing the [P4-programmable][2] data plane. It is a P4 program capable of emulating other P4 programs.
This repository is a modified version of the original HyPer4. We name it HyPer4+. It is focused on network slicing, with bandwidth restriction for slices
implemented. 

[References]: #

[1]: http://dl.acm.org/citation.cfm?id=2999607 "HyPer4: Using P4 to Virtualize the Programmable Data Plane"
[2]: http://arxiv.org/pdf/1312.1719.pdf "P4: Programming Protocol Independent Packet Processors"
